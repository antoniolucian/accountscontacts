public with sharing class AccountService {
    public AccountService() {}

    private AccountRepository accountRepository = new AccountRepository();
    private ContactRepository contactRepository = new ContactRepository();

    public Account insertOrUpdateAccount(Map<String, String> param) {
        Account account = (Account)JSON.deserialize(param.get('account'), Account.class);
        
        if(account.Id != null && !String.isBlank(account.Id)) {
           account = updateAccount(account);

        } else {
            account.Id = null;
           account = addAccount(account);
        }
        return account;
    }

    public Map<String, Object> manageAccountContact(Map<String, String> param){
        ContactService contactService = new ContactService();
        Account acc = insertOrUpdateAccount(param);
        List<Contact> listCnt = contactService.manageContact(param, acc);
        Map<String, Object> content = new Map<String, Object>();
        content.put('account', acc);
        content.put('contact', listCnt);
        return content;
    }

    public List<Account> addAccount(List<Account> accounts) {
        List<Account> accountInserted = new List<Account>();
        for(Account account : accounts) {
            accountInserted.add(addAccount(account));
        }
        return accountInserted;
    }

    public Account addAccount(Account account) {
        return accountRepository.saveAccount(account);
    }

    public List<Account> updateAccount(List<Account> accountsList) {
       List<Account> accountsUpdated = new List<Account>();
       for(Account account : accountsList) {
           accountsUpdated.add(updateAccount(account));
       }
       return accountsUpdated;
    }

    public Account updateAccount(Account account) {
        return accountRepository.updateAccount(account);
    }

    public void removeAccount(List<Account> accountsList) {
        for(Account account: accountsList) {
            removeAccount(account);
        }
    }

    public void removeAccount(Account account) {
        accountRepository.removeAccount(account);
    }
/*
    public List<Account> getAllAccount() {
        return accountRepository.getAllAccounts();
    } */

    public Map<String, Object> getAllAccount() {
        List<Account> accounts = accountRepository.getAllAccounts();
        Map<String, Object> content = new Map<String, Object>();
        content.put('account', accounts);
        return content;
    }

    public Map<String, Object> getAccountsService(Map<String, String> data) {
        Account account = (Account)JSON.deserialize(data.get('account'), Account.class);
        List<String> fields = new List<String>();
        fields.add('Name');
        fields.add('Phone');
        fields.add('Website');
        fields.add('BillingCity');
        Map<String, String> accountForRepo = new Map<String, String>();
        accountForRepo.put('Name', account.Name);
        accountForRepo.put('BillingCity', account.BillingCity);
        List<Account> accounts = accountRepository.getAccountsByCondition(fields, accountForRepo);
        System.debug('accounts from db : ' + accounts);
        Map<String, Object> content = new Map<String, Object>();
        content.put('account', accounts);
        return content;
    }

    public Account findAccountById(String accId){
        return accountRepository.findById(accId);
    }
    
    public Map<String, Object> getAccountById(String accId){
        Account acc = findAccountById(accId);
        List<Contact> contacts = contactRepository.findContactByAccountId(accId);
        Map<String, Object> content = new Map<String, Object>();
        content.put('account', acc);
        content.put('contacts', contacts);
        System.debug('content: ' + content);
        return content;
        }
}
