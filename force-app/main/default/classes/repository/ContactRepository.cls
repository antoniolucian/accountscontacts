public with sharing class ContactRepository {
    public ContactRepository() {}

    public List<Contact> findContactByAccountId(String accountId) {
        return [SELECT Id, AccountId, FirstName, LastName, Birthdate, Phone, Email, Fiscal_Code__c  FROM Contact WHERE AccountId = :accountId];
    } 

    public Contact saveContact(Contact contact) {
        insert contact;
        return contact;
    } 

    public Contact updateContact(Contact contact) {
        update contact;
        return contact;
    } 

    public void removeContact(Contact contact) {
        delete contact;
    } 
}
