public with sharing class ResponseDTO {
	@AuraEnabled
	public Map<String, Object> content = new Map<String, Object>();
	@AuraEnabled
	public String errorCode = '';
	@AuraEnabled
	public String errorMessage = '';
	@AuraEnabled
	public String message = '';
	@AuraEnabled
	public String messageCode = '';
	@AuraEnabled
    public Boolean status = false; // serve per vedere se e andato a buon fine 
}
