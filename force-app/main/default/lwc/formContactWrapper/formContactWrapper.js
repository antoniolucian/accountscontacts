import { LightningElement, api, track } from 'lwc';

export default class FormContactWrapper extends LightningElement {
    @api arrayForms = []; // lista contacts
    @api contactsid; // list contact id
    // @api : metodi/variabili visibili e richiamabili dal padre
    // @track : elementi visibili nel html


    @api
    getAllFields() { // result all forms
        const result = [];
        const elem = this.template.querySelectorAll("c-form-contact");
        console.log(this.arrayForms);
       /* if (this.contactsid.length > 0) { // set the contact id in the form
            this.arrayForms.forEach((element, index) => {
                elem[index].setContactId(this.contactsid[index]);
            });
        } */
        elem.forEach(element => {
            result.push(element.getFields()); // take all fields input of a single form
        });
        console.log(JSON.stringify(result));
        return result;
    }

    removeContact(event) {
        console.log("delete 2");
        console.log("id contact to remove: " + event.detail.ind);
        const onRemoveContact = new CustomEvent('contactremove', {detail: {ind: event.detail.ind}});
        // facciamo scattare l'evento, per fare questo si usa dispatchEvent()
        this.dispatchEvent(onRemoveContact);
    }

    @api
    validateAll() {
        const contactsFieldNotCompleted = [];
        this.template.querySelectorAll("c-form-contact").forEach(element => {
            let form = element.validate();
            if (form.length > 0) {
                contactsFieldNotCompleted.push(form);
            }
        });
        console.log(contactsFieldNotCompleted);
        return contactsFieldNotCompleted;
    }
/*
    @api
    editContacts() {
        console.log(JSON.stringify(this.arrayForms));
        const elem = this.template.querySelectorAll("c-form-contact");
        console.log(JSON.stringify(this.arrayForms));
        if (this.arrayForms.length > 0) {
            this.arrayForms.forEach((element, index) => {
                elem[index].editContact(element);
            });
        } 
        this.template.querySelectorAll("c-form-contact").forEach(element => {
            console.log("entra");
            element.editContact();
        });
    } */

    /*
    @api
    setContactsId(param) {
        this._contactsId = param;
        console.log(JSON.stringify(this.elements));
        this.elements.forEach((element, index) => {
            this.template.querySelectorAll("c-form-contact")[index].setContactId(param[index]);
        });
    } */
}