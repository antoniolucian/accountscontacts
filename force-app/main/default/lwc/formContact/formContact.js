import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { checkValability } from 'c/checkFiscalCode';

export default class FormContact extends LightningElement {
    @api contactinfo; // id: ...
    @track recordId; // for the template, set the id of current contact

    @track contact = {
        AccountId: "",
        Id: "",
        FirstName: "",
        LastName: "",
        Birthdate: "",
        Phone: "",
        Email: "",
        Fiscal_Code__c: ""
    }

    // called when the component is inserted in document
    connectedCallback() {
        this.editContact();
    }

    deleteContact() {
        console.log("delete 1");
        // ci dobbiamo creare un nostro evento custom (nomeEvento, {detail: data to send})
        const onRemoveContact = new CustomEvent('contactremove', { detail: { ind: this.contactinfo.Id } });
        // facciamo scattare l'evento, per fare questo si usa dispatchEvent()
        this.dispatchEvent(onRemoveContact);
    }

    @api
    getFields() {
        let clonedContact = { ...this.contact };
        try {
            const f = this.template.querySelector("[data-id='form']");
            f.querySelectorAll("lightning-input-field").forEach(obj => {
                if (obj.fieldName === 'Fiscal_Code__c') {
                    clonedContact[obj.fieldName] = obj.value.toUpperCase();
                } else {
                    clonedContact[obj.fieldName] = obj.value;
                }
            });
            checkValability(clonedContact, clonedContact.Fiscal_Code__c);
        } catch (error) {
            const event = new ShowToastEvent({
                title: 'Error',
                message: error,
                variant: 'error',
            });
            this.dispatchEvent(event);
            throw 'error';
        }
        return clonedContact;
    }

    @api
    validate() {
        const arr = [];
        this.template.querySelectorAll(".validate").forEach(obj => {
            console.log('Field name ' + obj.fieldName + 'Field value ' + obj.value);
            if (obj.value === '') {
                arr.push(obj.fieldName); // ritorna un array con i nomi dei field non completati
            }
        });
        return arr;
    }

    /*
    fiscalCodeValidation(fiscalCode) {
        var re = /^(?:[A-Z][AEIOU][AEIOUX]|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}(?:[\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[15MR][\dLMNP-V]|[26NS][0-8LMNP-U])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM]|[AC-EHLMPR-T][26NS][9V])|(?:[02468LNQSU][048LQU]|[13579MPRTV][26NS])B[26NS][9V])(?:[A-MZ][1-9MNP-V][\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$/i;
        if(fiscalCode.value != '') {
            var check = re.exec(fiscalCode.value);
            let response = (!check) ? fiscalCode.fieldName : '';
            return response;
        }else {
            return fiscalCode.fieldName;
        }
        
        
    } */


    // Scope: It's used to set the id of a contact
    @api
    setContactId(param) {
        console.log("param " + JSON.stringify(param));
        this.recordId = param.id;
        this.contact.Id = param.id;
        this.contact.AccountId = param.accountId;
        console.log("Contact " + JSON.stringify(this.contact));
    }

    @api
    editContact() {
        //console.log('contact in formContact edit ' + contact);
        console.log(JSON.stringify("formContact js " + this.contactinfo));
        if (this.contactinfo !== "") {
            this.contact = this.contactinfo;
            console.log(JSON.stringify(this.contact));
        }
    }

}