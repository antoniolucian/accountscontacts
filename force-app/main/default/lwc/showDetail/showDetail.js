import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';


const actions = [
    { label: 'Edit', name: 'edit' },
    { label: 'Show details', name: 'show_details' }
];

const columns = [
    { label: 'Account Name', fieldName: 'Name' },
    { label: 'Phone', fieldName: 'Phone', type: 'phone' },
    { label: 'Website', fieldName: 'Website', type: 'url' },
    { label: 'City', fieldName: 'BillingCity' },
    {
        type: 'action',
        typeAttributes: { rowActions: actions },
    },
];

export default class ShowDetail extends NavigationMixin(LightningElement) {
    @api result = [];

    columns = columns;

    @track recordId;

    handleRowAction(event) {
        const actionName = event.detail.action.name;
        //const row = event.detail.row.Id; // value of the row
        this.recordId = event.detail.row.Id;
        console.log("record Id : " + this.recordId);
        switch (actionName) {
            case 'edit':
                this.editRow();
                break;
            case 'show_details':
                this.showDetails();
                break;
            default:
        }
    }
    editRow() {
        // generate event
        // ci dobbiamo creare un nostro evento custom (nomeEvento, {detail: data to send})
        const accountId = new CustomEvent('getcurrentid', { detail: { ind: this.recordId } });
        // facciamo scattare l'evento, per fare questo si usa dispatchEvent()
        this.dispatchEvent(accountId);
    }

    showDetails() {
        const onShowDetails = new CustomEvent('showdetails', {detail: {ind:this.recordId}});
        this.dispatchEvent(onShowDetails);
    }
}