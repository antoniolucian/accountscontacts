import { LightningElement, track, api } from 'lwc';

export default class FormAccount extends LightningElement {

    //@track recordId = "";
    @api accountFromDb = "";

    @track account = {
        Id: "",
        Name: "",
        Phone: "",
        Website: "",
        BillingStreet: "",
        BillingCity: "",
        BillingCountry: ""
    }

    //@api accountid = "";
    @track accountid = "";

    connectedCallback() {
        this.accountid = (this.accountFromDb != null && this.accountFromDb != "") ? this.accountFromDb.Id : "";
    }

    @api
    refreshForm(event) {
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                this.accountid = "";
                field.reset();
            });
        }
    }


    @api
    getFields() {
        let clonedAcc = {...(this.accountFromDb !== "") ? this.accountFromDb : this.account};
        try {
            const f = this.template.querySelector("[data-id='form']");
            f.querySelectorAll("lightning-input-field").forEach(obj => {
                console.log(obj.value);
                //this.account[obj.fieldName] = obj.value;
                clonedAcc[obj.fieldName] = obj.value;
                console.log(JSON.stringify(clonedAcc));
            });
        } catch (error) {
            console.log(error);
        }
        //this.account.Id = this.accountFromDb.Id;
        //this.recordId = this.accountid;
        return clonedAcc;
    }

    @api
    validate() {
        const arr = [];
        this.template.querySelectorAll(".validate").forEach(obj => {
            console.log(obj.fieldName);
            console.log(obj.value);
            if (obj.value === '') {
                arr.push(obj.fieldName); // ritorna un array con i nomi dei field non completati
            }
        });
        return arr;
    }

    @api
    editAccount() {
        console.log(JSON.stringify(this.accountFromDb));
        if (this.accountFromDb !== "") {
            this.account = this.accountFromDb;
            console.log(JSON.stringify(this.account));
        }
    }
}