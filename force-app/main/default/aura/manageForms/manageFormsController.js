({
    init: function (component, event, helper) {
        helper.getCurrentAccountById(component, event, helper);
    },
    reInit: function (component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    addContact: function (component, event, helper) {
        helper.insertContact(component, event, helper);
    },
    removeContact: function (component, event, helper) {
        helper.deleteContact(component, event, helper);
    },
    newAccount: function (component, event, helper) {
        var refreshAccount = component.find('account').refreshForm();
        var refreshContacts = component.set("v.contacts", []);
    },

    save: function (component, event, helper) {
        helper.sendDataToServer(component, event, helper);
    }
})
