({
    getCurrentAccountById: function (component, event, helper) { // edit account
        var pageRef = component.get("v.pageReference");
        console.log('pageRef: ' + JSON.stringify(pageRef));
        if (pageRef != null) {
            var recordId = pageRef.state.c__recordId;
            console.log('c__recordId: ' + recordId);
            var action = component.get("c.getId");
            action.setParams({
                accId: recordId
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state == 'SUCCESS') {
                    var result = response.getReturnValue();
                    if (result.status) {
                        console.log('result: ' + result);
                        try {
                            component.set("v.account", result.content.account);
                            console.log(result.content.contacts);
                            component.set("v.contacts", result.content.contacts);
                            var editAccount = component.find("account").editAccount();
                            //var editContact = component.find("contact").editContacts();
                        } catch (e) {
                            console.log('errore');
                            this.messageAccount(component, event, helper, "Error!", "Error getting the Account.", 'error');
                        }
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },
    insertContact: function (component, event, helper) {
        var contacts = component.get("v.contacts");
        contacts.push({ Id: `fit${Date.now()}` });
        console.log(Date.now());
       // (contacts.length > 0) ? contacts.push("") : "";
        component.set("v.contacts", contacts);
    },
    deleteContact: function (component, event, helper) {
        var contacts = component.get("v.contacts");
        console.log("controller remove id elemento: " + event.getParam('ind')); // prendo il valore dell' aura:attribute array
        for (var cnt = 0; cnt < contacts.length; cnt++) {
            if (contacts[cnt].Id === event.getParam('ind')) {
                contacts.splice(contacts.indexOf(contacts[cnt]), 1);
            }
        }
        component.set("v.contacts", contacts); // nuovo valore dell' aura:attribute array
    },
    sendDataToServer: function (component, event, helper) {
        var account = component.find('account');
        var contacts = component.find('contact');
        var accountFieldsNotCompleted = account.validate();
        var contactsFieldsNotCompleted = contacts.validateAll();
        if (accountFieldsNotCompleted.length === 0 && contactsFieldsNotCompleted.length === 0) {
            // account.getFields().Id = accId;
            var userInfo = {
                account: JSON.stringify(account.getFields()),
                contact: JSON.stringify(contacts.getAllFields())
            };
            console.log(JSON.stringify(userInfo));
            // invia il json ad apex
            let action = component.get("c.sendAll");
            action.setParams({
                param: userInfo
            });
            // Add callback behavior for when response is received
            action.setCallback(this, function (response) {
                let state = response.getState();
                console.log(state);
                if (state === "SUCCESS") {
                    let responseResult = response.getReturnValue();
                    if (responseResult.status) {
                        console.log(responseResult.content.account);
                        try {
                            component.set("v.account", responseResult.content.account);
                            console.log(responseResult.content.account);
                        } catch (e) { }
                        var contactsFromDb = responseResult.content.contact;

                        if (contactsFromDb.length > 0) {
                          /*  var contactsIdFromDb = [];
                            for (var i = 0; i < contactsFromDb.length; i++) {
                                var c = contactsFromDb[i];
                                contactsIdFromDb.push(c);
                            } */
                            component.set("v.contacts", contactsFromDb);
                        }
                        if(userInfo.account.includes("Id")){
                            helper.messageAccount(component, event, helper, "Success!", "Account successfully edited.", "success");
                        } else {
                            helper.messageAccount(component, event, helper, "Success!", "Account successfully inserted.", "success");
                        }    
                    } else {
                        this.messageAccount(component, event, helper, "Error!", "Error in inserting Account.", 'error');
                    }
                } else {
                    this.messageAccount(component, event, helper, "Error!", "Error in inserting Account.", 'error');
                }
            });
            // Send action off to be executed
            $A.enqueueAction(action);
        } else {
            this.messageAccount(component, event, helper, "Error!", contactsFieldsNotCompleted.toString() + ' \nnot completed', 'error');
        }
    },
    messageAccount: function (component, event, helper, title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            message: message,
            type: type
        });
        toastEvent.fire();
    },

    messageContact: function (component, event, helper, title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            message: message,
            type: type
        });
        toastEvent.fire();
    },
})
