({
    getAllAccounts: function (component, event, helper) {
        let action = component.get("c.getAllAccounts");
        action.setCallback(this, function (response) {
            let state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                let responseResult = response.getReturnValue();
                if (responseResult.status) {
                    console.log(responseResult.content);
                    try {
                        component.set("v.dataFromDb", responseResult.content.account);
                    } catch (e) {
                        this.messageAccount(component, event, helper, 'Error', 'Error get Accounts', 'error');
                    }
                } else {
                    this.messageAccount(component, event, helper, 'Error', responseResult, 'error');
                }
            } else {
                this.messageAccount(component, event, helper, 'Error', 'Server Error', 'error');
            }
        });
        $A.enqueueAction(action);
    },

    sendData: function (component, event, helper) {
        var data = {
            account: JSON.stringify(component.get("v.data"))
        };
        let action = component.get("c.getAccounts");
        action.setParams({
            param: data
        });
        console.log(JSON.stringify(data));
        action.setCallback(this, function (response) {
            let state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                let responseResult = response.getReturnValue();
                if (responseResult.status) {
                    try {
                        component.set("v.dataFromDb", responseResult.content.account);
                    } catch (e) {
                        this.messageAccount(component, event, helper, 'Error', 'Error get Accounts', 'error');
                    }
                } else {
                    this.messageAccount(component, event, helper, 'Error', responseResult, 'error');
                }
            } else {
                this.messageAccount(component, event, helper, 'Error', 'Server Error', 'error');
            }
        });
        $A.enqueueAction(action);
    },
    messageAccount: function (component, event, helper, title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            message: message,
            type: type
        });
        toastEvent.fire();
    },
})
