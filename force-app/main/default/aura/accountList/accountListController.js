({
    init: function (component, event, helper) {
        // return all account for start app
        helper.getAllAccounts(component, event, helper);
    }, 
    getAccountId: function (component, event, helper) {
        var accoundId = event.getParam('ind');
        var navService = component.find("navService");
        var pageRef = {
            type : 'standard__component',
            attributes: {
                componentName: 'c__ManageForms'
            },
            state: {
                c__recordId: accoundId
            }
        };
        navService.navigate(pageRef);
    },
    showDetails: function (component, event, helper){
        var navService = component.find("navService");
        var accId = event.getParam('ind');
        var pageRef = {
            type: 'standard__recordPage',
            attributes: {
                recordId: accId,
                actionName: 'view'
            }
        };
        navService.navigate(pageRef);
    },
    checkInputData: function (component, event, helper) {
        var eventInput = event.getSource();
        var dataToSend = component.get("v.data");
        var nameOfEvent = eventInput.get("v.name");
        var valueOfEvent = eventInput.get("v.value");
        var data = {
            Name: '',
            BillingCity: ''
        };
        (dataToSend != null) ? data = dataToSend : '';
        if (valueOfEvent.length >= 3) {
            (nameOfEvent === "name") ? data.Name = valueOfEvent : data.BillingCity = valueOfEvent;
        } else {
            (nameOfEvent === "name") ? data.Name = '' : data.BillingCity = '';
        }
        component.set("v.data", data);
        helper.sendData(component, event, helper);
    },
})
